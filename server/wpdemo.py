"""gunicorn WSGI server configuration."""
from multiprocessing import cpu_count


def max_workers():
    return cpu_count() * 2 + 1


max_requests = 1000
worker_class = 'gevent'
workers = max_workers()

""" HIDE THIS """
raw_env = [
    "DJANGO_SETTINGS_MODULE=wpdemo.settings.base",
    "DATABASE_URL=postgres://lanshark:Rane4est@127.0.0.1/wpdemo",
]
