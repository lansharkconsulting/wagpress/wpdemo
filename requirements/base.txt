Django>=3.2,<3.3
django-csp==3.5
# django-debug-toolbar>=3.2,<4
django-dotenv==1.4.2
django-social-share==1.3.2
Pillow>=6.2.0,<6.3
puput==1.1.1
psycopg2>=2.7,<3.0
tapioca-disqus==0.1.2
wagtail>=2.13,<2.14
wagtailfontawesome>=1.1.3,<1.2
