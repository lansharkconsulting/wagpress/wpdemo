-r base.txt
elasticsearch==5.5.3
gunicorn==20.0.4
gevent==1.4.0
greenlet==0.4.15

# Additional dependencies for Heroku, AWS, and Google Cloud deployment
# boto3==1.9.189
# django-storages>=1.8,<1.9
# dj-database-url==0.4.1
# google-cloud-storage==1.20.0
psycopg2>=2.7,<3.0
uwsgi>=2.0.17,<2.1
whitenoise>=5.0,<5.1

# For retrieving credentials and signing requests to Elasticsearch
# aws-requests-auth==0.4.0
# botocore>=1.12.33,<1.13
# django_cache_url==2.0.0
# django-redis==4.11.0
