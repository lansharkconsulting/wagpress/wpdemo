WagPress demo project
=====================

This is a fork of the demonstration project for the amazing `Wagtail
CMS <https://github.com/wagtail/wagtail>`__. LANshark Consulting has
added the following:

-  automated generation of sitemaps.xml
-  integrated a bunch of css to make mobile/responsive better
-  swap meaning of PROJECT\_DIR and BASE\_DIR to match Wagtail 2.0
-  add production server files for gunicorn and gevent
-  improve hero image handling
-  convert more config/settings values to read from .env
-  add option to use contributed Postgres Search Backend
-  replaced the blog with Puput
-  updated to support ElasticSearch 6
-  configured to enable Disqus comments on blog
-  separated apps into individual components
-  update dependencies

The demo site is designed to provide examples of common features and
recipes to introduce you to Wagtail development. Beyond the code, it
will also let you explore the admin / editorial interface of the CMS.

**Document contents**

-  `Installation <#installation>`__
-  `Next steps <#next-steps>`__
-  `Contributing <#contributing>`__
-  `Other notes <#other-notes>`__

Installation
============

-  `Vagrant <#setup-with-vagrant>`__
-  `Docker <#setup-with-docker>`__
-  `Virtualenv <#setup-with-virtualenv>`__
-  `Heroku <#deploy-to-heroku>`__

If you're new to Python and/or Django, we suggest you run this project
on a Virtual Machine using Vagrant or Docker (whichever you're most
comfortable with). Both Vagrant and Docker will help resolve common
software dependency issues. Developers more familiar with virtualenv and
traditional Django app setup instructions should skip to `Setup with
virtualenv <#setup-with-virtualenv>`__.

Setup with Vagrant
------------------

Dependencies
~~~~~~~~~~~~

-  `Vagrant <https://www.vagrantup.com/>`__
-  `Virtualbox <https://www.virtualbox.org/>`__

Installation
~~~~~~~~~~~~

Once you've installed the necessary dependencies run the following
commands:

.. code:: bash

    git clone https://git@gitlab.com:/lansharkconsulting/wagpress/wpdemo.git
    cd wpdemo
    vagrant up
    vagrant ssh
    # then, within the SSH session:
    ./manage.py runserver 0.0.0.0:9000

The site will now be accessible at http://localhost:9000/ and the
Wagtail admin interface at http://localhost:9000/admin/.

Log into the admin with the credentials ``admin / changeme``.

Use ``Ctrl+c`` to stop the local server. To stop the Vagrant
environment, run ``exit`` then ``vagrant halt``.

Setup with Docker
-----------------

Dependencies
~~~~~~~~~~~~

-  `Docker <https://docs.docker.com/engine/installation/>`__
-  `Docker Compose <https://docs.docker.com/compose/install>`__

Installation
~~~~~~~~~~~~

Run the following commands:

.. code:: bash

    git clone https://git@gitlab.com:/lansharkconsulting/wagpress/wpdemo.git
    cd wpdemo
    docker-compose up --build -d
    docker-compose run app /venv/bin/python manage.py load_initial_data
    docker-compose up

The demo site will now be accessible at http://localhost:9000/ and the
Wagtail admin interface at http://localhost:9000/admin/.

Log into the admin with the credentials ``admin / changeme``.

**Important:** This ``docker-compose.yml`` is configured for local
testing only, and is *not* intended for production use.

Debugging
~~~~~~~~~

To tail the logs from the Docker containers in realtime, run:

.. code:: bash

    docker-compose logs -f

Setup with Virtualenv
---------------------

You can run the Wagtail demo locally without setting up Vagrant and
simply use Virtualenv, which is the `recommended installation
approach <https://docs.djangoproject.com/en/1.10/topics/install/#install-the-django-code>`__
for Django itself.

Dependencies
~~~~~~~~~~~~

-  `Virtualenv <https://virtualenv.pypa.io/en/stable/installation/>`__
-  `VirtualenvWrapper <https://virtualenvwrapper.readthedocs.io/en/latest/install.html>`__
   (optional)

Installation
~~~~~~~~~~~~

With `PIP <https://github.com/pypa/pip>`__ and
`virtualenvwrapper <https://virtualenvwrapper.readthedocs.io/en/latest/>`__
installed, run:

::

    mkvirtualenv wpdemo
    python --version

Confirm that this is showing a compatible version of Python 3.x. If not, and you have multiple versions of Python installed on your system, you may need to specify the appropriate version when creating the virtualenv:

    deactivate
    rmvirtualenv wpdemo
    mkvirtualenv wpdemo --python=python3.7
    python --version

Now we're ready to set up the wagpress demo project itself:

    cd ~/dev [or your preferred dev directory]
    git clone https://git@gitlab.com:/lansharkconsulting/wagpress/wpdemo.git
    cd wpdemo
    pip install -r requirements.txt

Next, we'll set up our local environment variables. We use
`django-dotenv <https://github.com/jpadilla/django-dotenv>`__ to help
with this. It reads environment variables located in a file name
``.env`` in the top level directory of the project. The only variable we
need to start is ``DJANGO_SETTINGS_MODULE``:

::

    $ cp wpdemo/settings/local.py.example wpdemo/settings/local.py
    $ echo "DJANGO_SETTINGS_MODULE=wpdemo.settings.dev" > .env

To set up your database and load initial data, run the following
commands:

::

    ./manage.py migrate
    ./manage.py load_initial_data
    ./manage.py runserver 0.0.0.0:9000

Log into the admin with the credentials ``admin / changeme``.

Deploy to Heroku
----------------

If you don't want to test locally you can deploy a demo site to a
publicly accessible server with `Heroku's <https://heroku.com>`__
one-click deployment solution to their free 'Hobby' tier:

|Deploy|

If you do not have a Heroku account, clicking the above button will walk
you through the steps to generate one. At this point you will be
presented with a screen to configure your app. For our purposes, we will
accept all of the defaults and click ``Deploy``. The status of the
deployment will dynamically update in the browser. Once finished, click
``View`` to see the public site.

Log into the admin with the credentials ``admin / changeme``.

To prevent the demo site from regenerating a new Django ``SECRET_KEY``
each time Heroku restarts your site, you should set a
``DJANGO_SECRET_KEY`` environment variable in Heroku using the web
interace or the
`CLI <https://devcenter.heroku.com/articles/heroku-cli>`__. If using the
CLI, you can set a ``SECRET_KEY`` like so:

::

    heroku config:set DJANGO_SECRET_KEY=changeme

To learn more about Heroku, read `Deploying Python and Django Apps on
Heroku <https://devcenter.heroku.com/articles/deploying-python>`__.

Storing Wagtail Media Files on AWS S3
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you have deployed the demo site to Heroku or via Docker, you may want
to perform some additional setup. Heroku uses an `ephemeral
filesystem <https://devcenter.heroku.com/articles/dynos#ephemeral-filesystem>`__,
and Docker-based hosting environments typically work in the same manner.
In laymen's terms, this means that uploaded images will disappear at a
minimum of once per day, and on each application deployment. To mitigate
this, you can host your media on S3.

This documentation assumes that you have an AWS account, an IAM user,
and a properly configured S3 bucket. These topics are outside of the
scope of this documentation; the following `blog
post <https://wagtail.io/blog/amazon-s3-for-media-files/>`__ will walk
you through those steps.

This demo site comes preconfigured with a production settings file that
will enable S3 for uploaded media storage if ``AWS_STORAGE_BUCKET_NAME``
is defined in the shell environment. All you need to do is set the
following environment variables. If using Heroku, you will first need to
install and configure the `Heroku
CLI <https://devcenter.heroku.com/articles/heroku-cli>`__. Then, execute
the following commands to set the aforementioned environment variables:

::

    heroku config:set AWS_STORAGE_BUCKET_NAME=changeme
    heroku config:set AWS_ACCESS_KEY_ID=changeme
    heroku config:set AWS_SECRET_ACCESS_KEY=changeme

Do not forget to replace the ``changeme`` with the actual values for
your AWS account. If you're using a different hosting environment, set
the same environment variables there using the method appropriate for
your environment.

Once Heroku restarts your application or your Docker container is
refreshed, you should have persistent media storage!

To copy the initial data included with this demo to the S3 bucket
(assuming you ran ``./manage.py load_initial_data`` per the above), you
can use the AWS CLI included with the requirements:

::

    heroku run aws s3 sync wpdemo/media/original_images/ s3://<bucket-name>/original_images/

Next steps
==========

Hopefully after you've experimented with the demo you'll want to create
your own site. To do that you'll want to run the ``wagtail start``
command in your environment of choice. You can find more information in
the `getting started Wagtail CMS
docs <http://wagtail.readthedocs.io/en/latest/getting_started/index.html>`__.

Contributing
============

If you're a Python or Django developer, fork the repo and get stuck in!
If you'd like to get involved you may find our `contributing
guidelines <https://gitlab.com:/lansharkconsulting/wagpress/wpdemo/blob/master/contributing.md>`__
a useful read.

Preparing this archive for distribution
---------------------------------------

If you change content or images in this repo and need to prepare a new
fixture file for export, do the following on a branch:

``./manage.py dumpdata --natural-foreign --indent 2 -e auth.permission -e contenttypes -e wagtailcore.GroupCollectionPermission -e wagtailsearch.querydailyhits -e wagtailsearch.query -e wagtailcore.pagerevision -e wagtailimages.rendition -e wagtailforms.formsubmission -e sessions > wpdemo/base/fixtures/wpdemo.json``

Please optimize any included images to 1200px wide with JPEG compression
at 60%. Note that ``media/images`` is ignored in the repo by
``.gitignore`` but ``media/original_images`` is not. Wagtail's local
image "renditions" are excluded in the fixture recipe above.

Make a pull request to
https://gitlab.com:/lansharkconsulting/wagpress/wpdemo

Other notes
===========

Note on search
--------------

Because we can't (easily) use ElasticSearch for this demo, we use
wagtail's native DB search. However, native DB search can't search
specific fields in our models on a generalized ``Page`` query. So for
demo purposes ONLY, we hard-code the model names we want to search into
``search.views``, which is not ideal. In production, use ElasticSearch
and a simplified search query, per
http://docs.wagtail.io/en/v1.13.1/topics/search/searching.html.

NOTE: we've replaced this with the new native Postgresql Search Engine.
This is the new default. Set POSTGRES\_SEARCH=True to enable, False to
disable.

Sending email from the contact form
-----------------------------------

The following setting in ``base.py`` and ``production.py`` ensures that
live email is not sent by the demo contact form.

``EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'``

In production on your own site, you'll need to change this to:

``EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'``

and configure `SMTP
settings <https://docs.djangoproject.com/en/1.10/topics/email/#smtp-backend>`__
appropriate for your email provider.

Ownership of demo content
-------------------------

All content in the demo is public domain. Textual content in this
project is either sourced from Wikipedia or is lorem ipsum. All images
are from either Wikimedia Commons or other copyright-free sources.

.. |Deploy| image:: https://www.herokucdn.com/deploy/button.svg
   :target: https://heroku.com/deploy?template=https://gitlab.com:/lansharkconsulting/wagpress/wpdemo
