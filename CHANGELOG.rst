**********
Change Log
**********

All enhancements and patches to wagpress-demo will be documented in this file. This project adheres to `Semantic Versioning <https://semver.org>`_.

[2020-01-04]
============

**Updated**

* to Wagtail 2.7

[2018-07-21]
============

**Added**

* LICENSE
* CODE_OF_CONDUCT
* CONTRIBUTING
* CONTRIBUTORS

**Changed**

* Updated the README
* Updated Project Features in the README
* Converted Blog to PUPUT

[2018-06-01]
============

initial commit (@lanshark)
